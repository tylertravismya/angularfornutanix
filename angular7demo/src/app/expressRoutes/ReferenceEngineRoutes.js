// referenceEngineRoutes.js

var express = require('express');
var app = express();
var referenceEngineRoutes = express.Router();

// Require Item model in our routes module
var ReferenceEngine = require('../models/ReferenceEngine');

// Defined store route
referenceEngineRoutes.route('/add').post(function (req, res) {
	var referenceEngine = new ReferenceEngine(req.body);
	referenceEngine.save()
    .then(item => {
    	res.status(200).json({'referenceEngine': 'ReferenceEngine added successfully'});
    })
    .catch(err => {
    	res.status(400).send("unable to save to database");
    });
});

// Defined get data(index or listing) route
referenceEngineRoutes.route('/').get(function (req, res) {
	ReferenceEngine.find(function (err, referenceEngines){
		if(err){
			console.log(err);
		}
		else {
			res.json(referenceEngines);
		}
	});
});

// Defined edit route
referenceEngineRoutes.route('/edit/:id').get(function (req, res) {
	var id = req.params.id;
	ReferenceEngine.findById(id, function (err, referenceEngine){
		res.json(referenceEngine);
	});
});

//  Defined update route
referenceEngineRoutes.route('/update/:id').post(function (req, res) {
	ReferenceEngine.findById(req.params.id, function(err, referenceEngine) {
		if (!referenceEngine)
			return next(new Error('Could not load a ReferenceEngine Document using id ' + req.params.id));
		else {
            referenceEngine.name = req.body.name;
            referenceEngine.active = req.body.active;
            referenceEngine.MainQuestionGroup = req.body.MainQuestionGroup;
            referenceEngine.Purpose = req.body.Purpose;

			referenceEngine.save().then(referenceEngine => {
				res.json('Update complete');
			})
			.catch(err => {
				res.status(400).send("unable to update the database");
			});
		}
	});
});

// Defined delete | remove | destroy route
referenceEngineRoutes.route('/delete/:id').get(function (req, res) {
   ReferenceEngine.findOneAndDelete({_id: req.params.id}, function(err, referenceEngine){
        if(err) res.json(err);
        else res.json('Successfully removed ' + ReferenceEngine + ' using id ' + req.params.id );
    });
});

module.exports = referenceEngineRoutes;