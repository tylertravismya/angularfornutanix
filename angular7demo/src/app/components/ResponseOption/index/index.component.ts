import { ResponseOptionService } from '../../../services/ResponseOption.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { ResponseOption } from '../../../models/ResponseOption';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexResponseOptionComponent implements OnInit {

  responseOptions: any;

  constructor(private http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: ResponseOptionService) {}

  ngOnInit() {
    this.getResponseOptions();
  }

  getResponseOptions() {
    this.service.getResponseOptions().subscribe(res => {
      this.responseOptions = res;
    });
  }

  deleteResponseOption(id) {
    this.service.deleteResponseOption(id)
		.then(success => 
			{
				this.router.navigateByUrl('/', {skipLocationChange: false}).then(()=>
							this.router.navigate(['indexResponseOption']));
			});  }
}
