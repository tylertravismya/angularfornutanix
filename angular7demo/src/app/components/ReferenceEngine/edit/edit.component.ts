import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReferenceEngineService } from '../../../services/ReferenceEngine.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { SubBaseComponent } from '../../ReferenceEngine/sub.base.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditReferenceEngineComponent extends SubBaseComponent implements OnInit {

  referenceEngine: any;
  referenceEngineForm: FormGroup;
  title = 'Edit ReferenceEngine';
  
  constructor( http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: ReferenceEngineService, 
  				private fb: FormBuilder) {
    super(http);
    this.createForm();
   }

  createForm() {
    this.referenceEngineForm = this.fb.group({
      name: ['', Validators.required],
      active: ['', Validators.required],
      MainQuestionGroup: ['', ],
      Purpose: ['', ]
   });
  }
  updateReferenceEngine(name, active, MainQuestionGroup, Purpose) {
    this.route.params.subscribe(params => {
    	this.service.updateReferenceEngine(name, active, MainQuestionGroup, Purpose, params['id'])
      		.then(success => this.router.navigate(['/indexReferenceEngine']) );
  });
}

// initialization
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.referenceEngine = this.service.editReferenceEngine(params['id']).subscribe(res => {
        this.referenceEngine = res;
      });
    });
    
    super.ngOnInit();
  }
}
