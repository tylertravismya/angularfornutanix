import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { QuestionService } from '../../../services/Question.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { BaseComponent } from '../../base.component';
import { SubBaseComponent } from '../../Question/sub.base.component';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateQuestionComponent extends SubBaseComponent implements OnInit {

  title = 'Add Question';
  questionForm: FormGroup;
  
  constructor(  http: HttpClient,
  				private questionservice: QuestionService, 
  				private fb: FormBuilder, 
  				private router: Router) {
	super(http);
    this.createForm();
   }
  createForm() {
    this.questionForm = this.fb.group({
      weight: ['', Validators.required],
      questionText: ['', Validators.required],
      identifier: ['', Validators.required],
      mustBeAnswered: ['', Validators.required],
      responseExclusive: ['', Validators.required],
      Responses: ['', ]
   });
  }
  addQuestion(weight, questionText, identifier, mustBeAnswered, responseExclusive, Responses) {
      this.questionservice.addQuestion(weight, questionText, identifier, mustBeAnswered, responseExclusive, Responses)
      	.then(success => this.router.navigate(['/indexQuestion']) );
  }
  
// initialization  
  ngOnInit() {
  	super.ngOnInit();
  }
}
