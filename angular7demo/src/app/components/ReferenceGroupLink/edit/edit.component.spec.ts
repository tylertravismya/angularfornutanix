import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { EditReferenceGroupLinkComponent } from './edit.component';

describe('EditReferenceGroupLinkComponent', () => {
  let component: EditReferenceGroupLinkComponent;
  let fixture: ComponentFixture<EditReferenceGroupLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [ EditReferenceGroupLinkComponent ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditReferenceGroupLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
