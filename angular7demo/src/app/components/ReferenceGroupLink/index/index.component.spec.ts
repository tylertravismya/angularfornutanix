import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { IndexReferenceGroupLinkComponent } from './index.component';

describe('IndexReferenceGroupLinkComponent', () => {
  let component: IndexReferenceGroupLinkComponent;
  let fixture: ComponentFixture<IndexReferenceGroupLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [ IndexReferenceGroupLinkComponent ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexReferenceGroupLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
