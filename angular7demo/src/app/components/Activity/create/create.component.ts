import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivityService } from '../../../services/Activity.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { BaseComponent } from '../../base.component';
import { SubBaseComponent } from '../../Activity/sub.base.component';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateActivityComponent extends SubBaseComponent implements OnInit {

  title = 'Add Activity';
  activityForm: FormGroup;
  
  constructor(  http: HttpClient,
  				private activityservice: ActivityService, 
  				private fb: FormBuilder, 
  				private router: Router) {
	super(http);
    this.createForm();
   }
  createForm() {
    this.activityForm = this.fb.group({
      refObjId: ['', Validators.required],
      createDateTime: ['', Validators.required],
      Type: ['', Validators.required],
      User: ['', ]
   });
  }
  addActivity(refObjId, createDateTime, Type, User) {
      this.activityservice.addActivity(refObjId, createDateTime, Type, User)
      	.then(success => this.router.navigate(['/indexActivity']) );
  }
  
// initialization  
  ngOnInit() {
  	super.ngOnInit();
  }
}
