import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ReferenceGiverService } from '../../../services/ReferenceGiver.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { BaseComponent } from '../../base.component';
import { SubBaseComponent } from '../../ReferenceGiver/sub.base.component';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateReferenceGiverComponent extends SubBaseComponent implements OnInit {

  title = 'Add ReferenceGiver';
  referenceGiverForm: FormGroup;
  
  constructor(  http: HttpClient,
  				private referenceGiverservice: ReferenceGiverService, 
  				private fb: FormBuilder, 
  				private router: Router) {
	super(http);
    this.createForm();
   }
  createForm() {
    this.referenceGiverForm = this.fb.group({
      dateLastUpdated: ['', Validators.required],
      active: ['', Validators.required],
      dateTimeLastViewedExternally: ['', Validators.required],
      makeViewableToUser: ['', Validators.required],
      dateLastSent: ['', Validators.required],
      numDaysToExpire: ['', Validators.required],
      QuestionGroup: ['', ],
      Status: ['', ],
      Type: ['', ],
      User: ['', ],
      Referrer: ['', ],
      Answers: ['', ],
      LastQuestionAnswered: ['', ]
   });
  }
  addReferenceGiver(dateLastUpdated, active, dateTimeLastViewedExternally, makeViewableToUser, dateLastSent, numDaysToExpire, QuestionGroup, Status, Type, User, Referrer, Answers, LastQuestionAnswered) {
      this.referenceGiverservice.addReferenceGiver(dateLastUpdated, active, dateTimeLastViewedExternally, makeViewableToUser, dateLastSent, numDaysToExpire, QuestionGroup, Status, Type, User, Referrer, Answers, LastQuestionAnswered)
      	.then(success => this.router.navigate(['/indexReferenceGiver']) );
  }
  
// initialization  
  ngOnInit() {
  	super.ngOnInit();
  }
}
