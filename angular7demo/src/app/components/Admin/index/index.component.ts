import { AdminService } from '../../../services/Admin.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Admin } from '../../../models/Admin';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexAdminComponent implements OnInit {

  admins: any;

  constructor(private http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: AdminService) {}

  ngOnInit() {
    this.getAdmins();
  }

  getAdmins() {
    this.service.getAdmins().subscribe(res => {
      this.admins = res;
    });
  }

  deleteAdmin(id) {
    this.service.deleteAdmin(id)
		.then(success => 
			{
				this.router.navigateByUrl('/', {skipLocationChange: false}).then(()=>
							this.router.navigate(['indexAdmin']));
			});  }
}
