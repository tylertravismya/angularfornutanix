var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for ResponseOption
var ResponseOption = new Schema({
  responseText: {
	type : String
  },
  gotoQuestionId: {
	type : String
  },
},{
    collection: 'responseOptions'
});

module.exports = mongoose.model('ResponseOption', ResponseOption);