import { TestBed } from '@angular/core/testing';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { ReferenceGiverService } from './ReferenceGiver.service';

describe('ReferenceGiverService', () => {
  	beforeEach(() => {
	  TestBed.configureTestingModule({ imports: [HttpClient, FormGroup, FormBuilder, Validators], providers: [ReferenceGiverService] });
	});

  it('should be created', () => {
    const service: ReferenceGiverService = TestBed.get(ReferenceGiverService);
    expect(service).toBeTruthy();
  });
});
