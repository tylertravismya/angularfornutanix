import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import {ReferenceGiver} from '../models/ReferenceGiver';
import {QuestionGroupService} from '../services/QuestionGroup.service';
import {UserService} from '../services/User.service';
import {ReferrerService} from '../services/Referrer.service';
import {AnswerService} from '../services/Answer.service';
import {QuestionService} from '../services/Question.service';
import { BaseService } from './base.service';

@Injectable()
export class ReferenceGiverService extends BaseService {

	//********************************************************************
	// general holder 
	//********************************************************************
	referenceGiver : any;
	
	//********************************************************************
	// Catch all for the return value of a service call
	//********************************************************************
	result: any;

	//********************************************************************
	// sole constructor, injected with the HttpClient
	//********************************************************************
 	constructor(private http: HttpClient) {
 	    super();
    }
 	
	//********************************************************************
	// add a ReferenceGiver 
	// returns the results untouched as a JSON representation 
	// delegates via URI to an ORM handler
	//********************************************************************
  	addReferenceGiver(dateLastUpdated, active, dateTimeLastViewedExternally, makeViewableToUser, dateLastSent, numDaysToExpire, QuestionGroup, Status, Type, User, Referrer, Answers, LastQuestionAnswered) : Promise<any> {
    	const uri = this.ormUrl + '/ReferenceGiver/add';
    	const obj = {
      		dateLastUpdated: dateLastUpdated,
      		active: active,
      		dateTimeLastViewedExternally: dateTimeLastViewedExternally,
      		makeViewableToUser: makeViewableToUser,
      		dateLastSent: dateLastSent,
      		numDaysToExpire: numDaysToExpire,
      		QuestionGroup: QuestionGroup != null && QuestionGroup.length > 0 ? QuestionGroup : null,
      		Status: Status,
      		Type: Type,
      		User: User != null && User.length > 0 ? User : null,
      		Referrer: Referrer != null && Referrer.length > 0 ? Referrer : null,
      		Answers: Answers != null && Answers.length > 0 ? Answers : null,
			LastQuestionAnswered: LastQuestionAnswered != null && LastQuestionAnswered.length > 0 ? LastQuestionAnswered : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// gets all ReferenceGiver 
	// returns the results untouched as JSON representation of an
	// array of ReferenceGiver models
	// delegates via URI to an ORM handler
	//********************************************************************
	getReferenceGivers() {
    	const uri = this.ormUrl + '/ReferenceGiver';
    	
    	return this
            	.http.get(uri).map(res => {
              						return res;
            					});
  	}

	//********************************************************************
	// edit a ReferenceGiver 
	// returns the results untouched as a JSON representation of a
	// ReferenceGiver model
	// delegates via URI to an ORM handler
	//********************************************************************
  	editReferenceGiver(id) {
    	const uri = this.ormUrl + '/ReferenceGiver/edit/' + id;
    	
    	return this.http.get(uri).map(res => {
              							return res;
            						});
  	}

	//********************************************************************
	// update a ReferenceGiver 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	updateReferenceGiver(dateLastUpdated, active, dateTimeLastViewedExternally, makeViewableToUser, dateLastSent, numDaysToExpire, QuestionGroup, Status, Type, User, Referrer, Answers, LastQuestionAnswered, id)  : Promise<any>  {
    	const uri = this.ormUrl + '/ReferenceGiver/update/' + id;
    	const obj = {
      		dateLastUpdated: dateLastUpdated,
      		active: active,
      		dateTimeLastViewedExternally: dateTimeLastViewedExternally,
      		makeViewableToUser: makeViewableToUser,
      		dateLastSent: dateLastSent,
      		numDaysToExpire: numDaysToExpire,
      		QuestionGroup: QuestionGroup != null && QuestionGroup.length > 0 ? QuestionGroup : null,
      		Status: Status,
      		Type: Type,
      		User: User != null && User.length > 0 ? User : null,
      		Referrer: Referrer != null && Referrer.length > 0 ? Referrer : null,
      		Answers: Answers != null && Answers.length > 0 ? Answers : null,
			LastQuestionAnswered: LastQuestionAnswered != null && LastQuestionAnswered.length > 0 ? LastQuestionAnswered : null
    	};
    	
    	return this.http.post(uri, obj).toPromise();
  	}

	//********************************************************************
	// delete a ReferenceGiver 
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	deleteReferenceGiver(id)  : Promise<any> {
    	const uri = this.ormUrl + '/ReferenceGiver/delete/' + id;

        return this.http.get(uri).toPromise();
  }
  
    		//********************************************************************
	// assigns a QuestionGroup on a ReferenceGiver
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	assignQuestionGroup( referenceGiverId, questionGroupId ): Promise<any> {

		// get the ReferenceGiver from storage
		this.loadHelper( referenceGiverId );
		
		// get the QuestionGroup from storage
		var questionGroup 	= new QuestionGroupService(this.http).editQuestionGroup(questionGroupId);
		
		// assign the QuestionGroup		
		this.referenceGiver.questionGroup = questionGroup;
      		
		// save the ReferenceGiver
		return this.saveHelper();		
	}

	//********************************************************************
	// unassigns a QuestionGroup on a ReferenceGiver
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	unassignQuestionGroup( referenceGiverId ): Promise<any> {

		// get the ReferenceGiver from storage
        this.loadHelper( referenceGiverId );
		
		// assign QuestionGroup to null		
		this.referenceGiver.questionGroup = null;
      		
		// save the ReferenceGiver
		return this.saveHelper();
	}
	
	//********************************************************************
	// assigns a User on a ReferenceGiver
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	assignUser( referenceGiverId, userId ): Promise<any> {

		// get the ReferenceGiver from storage
		this.loadHelper( referenceGiverId );
		
		// get the User from storage
		var user 	= new UserService(this.http).editUser(userId);
		
		// assign the User		
		this.referenceGiver.user = user;
      		
		// save the ReferenceGiver
		return this.saveHelper();		
	}

	//********************************************************************
	// unassigns a User on a ReferenceGiver
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	unassignUser( referenceGiverId ): Promise<any> {

		// get the ReferenceGiver from storage
        this.loadHelper( referenceGiverId );
		
		// assign User to null		
		this.referenceGiver.user = null;
      		
		// save the ReferenceGiver
		return this.saveHelper();
	}
	
	//********************************************************************
	// assigns a Referrer on a ReferenceGiver
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	assignReferrer( referenceGiverId, referrerId ): Promise<any> {

		// get the ReferenceGiver from storage
		this.loadHelper( referenceGiverId );
		
		// get the Referrer from storage
		var referrer 	= new ReferrerService(this.http).editReferrer(referrerId);
		
		// assign the Referrer		
		this.referenceGiver.referrer = referrer;
      		
		// save the ReferenceGiver
		return this.saveHelper();		
	}

	//********************************************************************
	// unassigns a Referrer on a ReferenceGiver
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	unassignReferrer( referenceGiverId ): Promise<any> {

		// get the ReferenceGiver from storage
        this.loadHelper( referenceGiverId );
		
		// assign Referrer to null		
		this.referenceGiver.referrer = null;
      		
		// save the ReferenceGiver
		return this.saveHelper();
	}
	
	//********************************************************************
	// assigns a LastQuestionAnswered on a ReferenceGiver
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************
	assignLastQuestionAnswered( referenceGiverId, lastQuestionAnsweredId ): Promise<any> {

		// get the ReferenceGiver from storage
		this.loadHelper( referenceGiverId );
		
		// get the Question from storage
		var question 	= new QuestionService(this.http).editQuestion(lastQuestionAnsweredId);
		
		// assign the LastQuestionAnswered		
		this.referenceGiver.lastQuestionAnswered = question;
      		
		// save the ReferenceGiver
		return this.saveHelper();		
	}

	//********************************************************************
	// unassigns a LastQuestionAnswered on a ReferenceGiver
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	unassignLastQuestionAnswered( referenceGiverId ): Promise<any> {

		// get the ReferenceGiver from storage
        this.loadHelper( referenceGiverId );
		
		// assign LastQuestionAnswered to null		
		this.referenceGiver.lastQuestionAnswered = null;
      		
		// save the ReferenceGiver
		return this.saveHelper();
	}
	

	//********************************************************************
	// adds one or more answersIds as a Answers 
	// to a ReferenceGiver
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************				
	addAnswers( referenceGiverId, answersIds ): Promise<any> {

		// get the ReferenceGiver
		this.loadHelper( referenceGiverId );
				
		// split on a comma with no spaces
		var idList = answersIds.split(',')

		// iterate over array of answers ids
		idList.forEach(function (id) {
			// read the Answer		
			var answer = new AnswerService(this.http).editAnswer(id);	
			// add the Answer if not already assigned
			if ( this.referenceGiver.answers.indexOf(answer) == -1 )
				this.referenceGiver.answers.push(answer);
		});
				
		// save it		
		return this.saveHelper();
	}			
	
	//********************************************************************
	// removes one or more answersIds as a Answers 
	// from a ReferenceGiver
	// returns a Promise
	// delegates via URI to an ORM handler
	//********************************************************************						
	removeAnswers( referenceGiverId, answersIds ): Promise<any> {
		
		// get the ReferenceGiver
		this.loadHelper( referenceGiverId );

				
		// split on a comma with no spaces
		var idList 					= answersIds.split(',');
		var answers 	= this.referenceGiver.answers;
		
		if ( answers != null && answersIds != null ) {
		
			// iterate over array of answers ids
			answers.forEach(function (obj) {				
				if ( answersIds.indexOf(obj._id) > -1 ) {
					 // remove the Answer
					this.referenceGiver.answers.pop(obj);
				}
			});
					
		    // save it		
			return this.saveHelper();
		}
	}
			

	//********************************************************************
	// saveHelper - internal helper to save a ReferenceGiver
	//********************************************************************
	saveHelper() : Promise<any> {
		
		const uri = this.ormUrl + '/ReferenceGiver/update/' + this.referenceGiver._id;		
		
    	return this
      			.http
      			.post(uri, this.referenceGiver)
				.toPromise();			
	}

	//********************************************************************
	// loadHelper - internal helper to load a ReferenceGiver
	//********************************************************************	
	loadHelper( id ) {
		this.editReferenceGiver(id)
        		.subscribe(res => {
        			this.referenceGiver = res;
      			});
	}
}