import { TestBed } from '@angular/core/testing';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { QuestionGroupService } from './QuestionGroup.service';

describe('QuestionGroupService', () => {
  	beforeEach(() => {
	  TestBed.configureTestingModule({ imports: [HttpClient, FormGroup, FormBuilder, Validators], providers: [QuestionGroupService] });
	});

  it('should be created', () => {
    const service: QuestionGroupService = TestBed.get(QuestionGroupService);
    expect(service).toBeTruthy();
  });
});
