// server.js
const express = require('express'),
	path = require('path'),
	bodyParser = require('body-parser'), 
	cors = require('cors'),
	mongoose = require('mongoose'),
    userRoutes = require('./src/app/expressRoutes/UserRoutes'),
    referrerRoutes = require('./src/app/expressRoutes/ReferrerRoutes'),
    referenceGiverRoutes = require('./src/app/expressRoutes/ReferenceGiverRoutes'),
    referrerGroupRoutes = require('./src/app/expressRoutes/ReferrerGroupRoutes'),
    referenceEngineRoutes = require('./src/app/expressRoutes/ReferenceEngineRoutes'),
    questionRoutes = require('./src/app/expressRoutes/QuestionRoutes'),
    responseOptionRoutes = require('./src/app/expressRoutes/ResponseOptionRoutes'),
    questionGroupRoutes = require('./src/app/expressRoutes/QuestionGroupRoutes'),
    adminRoutes = require('./src/app/expressRoutes/AdminRoutes'),
    activityRoutes = require('./src/app/expressRoutes/ActivityRoutes'),
    commentRoutes = require('./src/app/expressRoutes/CommentRoutes'),
    answerRoutes = require('./src/app/expressRoutes/AnswerRoutes'),
    referenceGroupLinkRoutes = require('./src/app/expressRoutes/ReferenceGroupLinkRoutes'),
	config = require('./config/mongoDb.js');

mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGO_HOST_ADDRESS || config.DB, {
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 1000
  }).then(
    () => {console.log('Database is connected') },
    err => { console.log('Can not connect to the database: ' + err)}
  );

const app = express();
app.use(bodyParser.json());
app.use(cors());
const port = 4000;

app.use('/User', userRoutes);
app.use('/Referrer', referrerRoutes);
app.use('/ReferenceGiver', referenceGiverRoutes);
app.use('/ReferrerGroup', referrerGroupRoutes);
app.use('/ReferenceEngine', referenceEngineRoutes);
app.use('/Question', questionRoutes);
app.use('/ResponseOption', responseOptionRoutes);
app.use('/QuestionGroup', questionGroupRoutes);
app.use('/Admin', adminRoutes);
app.use('/Activity', activityRoutes);
app.use('/Comment', commentRoutes);
app.use('/Answer', answerRoutes);
app.use('/ReferenceGroupLink', referenceGroupLinkRoutes);

const server = app.listen(port, function(){
  console.log('Listening on port ' + port);
});
